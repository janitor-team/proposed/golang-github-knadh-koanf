Source: golang-github-knadh-koanf
Section: devel
Priority: optional
Maintainer: Debian Go Packaging Team <team+pkg-go@tracker.debian.org>
Uploaders: Thorsten Alteholz <debian@alteholz.de>
Build-Depends: debhelper-compat (= 12),
               dh-golang,
               golang-any,
               golang-github-fatih-structs-dev,
               golang-github-fsnotify-fsnotify-dev,
               golang-github-hashicorp-hcl-dev,
               golang-github-mitchellh-mapstructure-dev,
               golang-github-pelletier-go-toml-dev,
               golang-github-rhnvrm-simples3-dev,
               golang-github-spf13-pflag-dev,
               golang-github-stretchr-testify-dev,
               golang-yaml.v2-dev
Standards-Version: 4.5.0
Homepage: https://github.com/knadh/koanf
Vcs-Browser: https://salsa.debian.org/go-team/packages/golang-github-knadh-koanf
Vcs-Git: https://salsa.debian.org/go-team/packages/golang-github-knadh-koanf.git
XS-Go-Import-Path: github.com/knadh/koanf
Testsuite: autopkgtest-pkg-go

Package: golang-github-knadh-koanf-dev
Architecture: all
Depends: ${misc:Depends},
         golang-github-fatih-structs-dev,
         golang-github-fsnotify-fsnotify-dev,
         golang-github-hashicorp-hcl-dev,
         golang-github-mitchellh-mapstructure-dev,
         golang-github-pelletier-go-toml-dev,
         golang-github-rhnvrm-simples3-dev,
         golang-github-spf13-pflag-dev,
         golang-github-stretchr-testify-dev,
         golang-yaml.v2-dev
Description: extensible library for reading config (file, S3 etc.) in Go applications
 This package, koanf (pronounced conf; a play on the Japanese Koan) is
 a library for reading configuration from different sources in different
 formats in Go applications. It is a cleaner, lighter alternative to
 spf13/viper with better abstractions and extensibility and fewer
 dependencies.
 .
 koanf comes with built in support for reading configuration from files,
 command line flags, and environment variables, and can parse JSON, YAML,
 TOML, and Hashicorp HCL.
